package io.github.terentiyp.school;

import io.github.terentiyp.school.enums.Course;
import io.github.terentiyp.school.enums.YearOfUniversity;
import io.github.terentiyp.school.model.Student;
import io.github.terentiyp.school.model.Teacher;

import java.util.Date;
import java.util.Scanner;

public class Main {
    Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        System.out.println("Who would you like to contribute?\n");
        Scanner scanner = new Scanner(System.in);
        String object = scanner.nextLine();
        switch (object.toUpperCase()) {
            case ("STUDENT"):
                Student student = createStudent(scanner);
                break;
            case ("TEACHER"):
               Teacher teacher = createTeacher(scanner);
                break;
            default:
                System.out.println("error ");
        }

    }

    private static Student createStudent(Scanner scanner) {
        System.out.println("Hello, please create student record");
        System.out.println("Type name: ");
        String name = scanner.nextLine();
        System.out.println("Type date of bird in the following format dd.mm.yyyy: ");
        String[] split = scanner.nextLine().split("\\.");
        int year = Integer.parseInt(split[2]);
        int month = Integer.parseInt(split[1]);
        int day = Integer.parseInt(split[0]);

        System.out.println("Type year of university: ");
        String yearOfUnivInput =  scanner.nextLine();
        YearOfUniversity yearOfStudy = null;

        switch (yearOfUnivInput.toUpperCase()) {
            case ("FIRST"):
                yearOfStudy = YearOfUniversity.FIRST;
                break;
            case ("SECOND"):
                yearOfStudy = YearOfUniversity.SECOND;
                break;
            case ("THIRD"):
                yearOfStudy = YearOfUniversity.THIRD;
                break;
            case ("FOURTH"):
                yearOfStudy = YearOfUniversity.FOURTH;
                break;
            case ("FIFTH"):
                yearOfStudy = YearOfUniversity.FIFTH;
                break;
            default:
                System.out.println("error ");
        }



        /*YearOfUniversity studyInput = null;
        switch (study){
            case  (FIRST):*/
        return new Student(name, new Date(year, month, day), yearOfStudy);


    }

    private static Teacher createTeacher(Scanner scanner) {
        System.out.println("Hello, please create teacher record");
        System.out.println("Type name: ");
        String name = scanner.nextLine();
        System.out.println("Type date of bird in the following format dd.mm.yyyy: ");
        String[] split = scanner.nextLine().split("\\.");
        int year = Integer.parseInt(split[2]);
        int month = Integer.parseInt(split[1]);
        int day = Integer.parseInt(split[0]);

        System.out.println("Type course: ");
        String courseInput =  scanner.nextLine();
        Course course = null;

        switch (courseInput.toUpperCase()) {
            case ("MATHEMATICS"):
                course = Course.MATHEMATICS;
                break;
            case ("PHYSICS"):
                course = Course.PHYSICS;
                break;
            case ("ENGLISH"):
                course = Course.ENGLISH;
                break;
            case ("ECONOMICS"):
                course = Course.ECONOMICS;
                break;
            default:
                System.out.println("error ");
        }

        return new Teacher(name, new Date(year, month, day), course);


    }


}
